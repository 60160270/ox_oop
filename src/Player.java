
public class Player {
	private char name ;
	private int win;
	private int draw;
	private int lose;
	Player(char name){
		this.name = name ;
		this.win = 0;
		this.lose = 0 ;
		this.draw = 0 ;
	}
	char getName(){
		return this.name;
	}
	int getWin() {
		return this.win;
	}
	int getDraw() {
		return this.draw;
	}
	int getLose() {
		return this.lose;
	}
	void addWin() {
		this.win++;
	}
	void addLose() {
		this.lose++;
	}
	void addDraw() {
		this.draw++;
	}
	
	
}
